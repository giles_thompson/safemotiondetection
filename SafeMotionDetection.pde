/**
 * A Processing Motion Detection application.
 *
 * Essentially treats each pixel in the video input source a point 3D space,
 * with each axis set equivalent to the the respective RGB 
 * component values of the pixel. We then proceed to compute the 
 * Euclidean Distance between the pixels in current and immediately 
 * preceeding frame, we determine that "motion" has occured where the
 * delta between these static frames (current and previous) exceeds 
 * the prevailing user-defined threshold.
 * 
 * @Author Giles Thompson
 */

import processing.video.*;
import controlP5.*;

//var to hold capture component.
Capture cam;

//window/video dimensions
int width = 640;
int height = 480;

//var to hold controller
ControlP5 myController;

//var to store previous image read from the camera source.
PImage prev;

//var to store permissible thresehold
float threshold = 50;



void setup() {
  
  //set windows size
  size(640,480);
  
  //attempt to acquire camera
  cam = this.acquireCamera(width,height);
  
  //create empty image
  prev = createImage(640,480,RGB);
  
  
  //set up controller
  myController = new ControlP5(this);
  
  //create slider
  myController.addSlider("Threshold",0,375,50,20,20,100,10);
  
  //set slider label colour
  myController.get("Threshold").setColorLabel(#0000ff);
  
}



void draw() {
  
  //update the threshold if necessary..
  if(threshold != myController.get("Threshold").getValue())
    threshold = myController.get("Threshold").getValue();
  
  
  //grab the current cam current frame pixels.
  cam.loadPixels();
  
  //grab the cam previous frame pixels
  prev.loadPixels();
  
  //draw the current cam pixels to the screen.
  image(cam, 0, 0);
  
  //iterate over each of the pixels of the current frame and 
  //compare them to those in the frame immediately preceeding this one
  loadPixels();
  for(int x = 0; x < cam.width; x++){
     for(int y= 0; y < cam.height; y++){
       
       //grab the pixel at the current location..
       int curLoc = x + y * cam.width;
       
       //grab and decompose the CURRENT pixel of the CURRENT frame into its RGB components
       color curColor = cam.pixels[curLoc]; 
       float r1 = red(curColor);
       float g1 = green(curColor);
       float b1 = blue(curColor);
       
       //grab decompose the CURRENT pixel of the PREVIOUS frame into its RGB components.
       color prevColor = prev.pixels[curLoc];
       float r2 = red(prevColor);
       float g2 = green(prevColor);
       float b2 = blue(prevColor);
       
       //calculate the delta in the distance between the pixel at the current and previous frame.
       float distDelta = this.pixelDistCalc(r1,g1,b1,r2,g2,b2);
       
       //if the delta in the distance exceeds our predetermined threshold 
       //squared (on account of not having Rooted the distance calc) then...
       if(distDelta >  threshold*threshold){
         
         pixels[curLoc] = color(0);
         
         //store the current frame to our buffer
         //another thread will take care of compiling the raw frames into
         //a video clip.
         
       }
       else{
         pixels[curLoc] = color(255);
       }
       
      
     }
  }
  
  updatePixels();
 
}

void captureEvent(Capture c){
  prev.copy(c,0,0,c.width,c.height,0,0,prev.width,prev.height);
  prev.updatePixels();
  c.read();
  
  
}



                              //private helper methods
                              
                              

                              

/**  
    Calculates the distance between two pixels in 3 Dimentional (colour) space 
    the Root of this Euclidian distance calculation SHOULD be taken however
    we opt here to square the threshold calculation to account for this further up the stack
    thereby negating the need for the root which should yield faster execution times.
 */
private float pixelDistCalc(float x1,float y1,float z1,float x2,float y2,float z2){
  float dist = ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) + ((z2-z1) *(z2-z1));
  return dist;
}
                              

 private Capture acquireCamera(int camWidth,int camHeight){
  
  //attempt to grab a list of cameras..
  String[] cameras = Capture.list();
  
  //var to store camera
  Capture selectedCam = null;

  if (cameras == null || cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } 
  else {
    
    println("Available cameras:");
    printArray(cameras);

    // The camera can be initialized directly using an element
    // from the array returned by list():
    selectedCam = new Capture(this,camWidth,camHeight,cameras[0]);
    // Or, the settings can be defined based on the text in the list
    //cam = new Capture(this, 640, 480, "Built-in iSight", 30);
    
    // Start capturing the images from the camera
    selectedCam.start();
  }
  
  return selectedCam;
}
